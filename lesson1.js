let angle = 0;
let minimumDistanceMouse = 50;

function setup() {
  createCanvas(400, 400);
  rectMode(CENTER);
  angleMode(DEGREES);
}

function draw() {
  let rectangleCenter = [width / 2, height / 2];
  let distance = dist(mouseX, mouseY, ...rectangleCenter);

  if (distance < minimumDistanceMouse) {
    angle += 1;
  }

  background(220);
  translate(width / 2, height / 2);
  rotate(angle);
  rect(0, 0, 100, 50);
}
